from django.shortcuts import render
from django.views.generic import TemplateView
# importing the module
import imdb
import random
import datetime
import time
from django.shortcuts import render
from plotly.offline import plot
from plotly.graph_objs import Scatter

# Create your views here.
class HomePageView(TemplateView):
    def get(self, request):
        data = {}

        z_data = []
        y_data = []
        x_data = []

        # creating instance of IMDb
        ia = imdb.IMDb()

        # getting top 250 movies
        search = ia.get_top250_movies()

        # printing only first 250 movies title
        for i in range(250):
            z_data.append(search[i]['title'])
            y_data.append(search[i]['rating'])
            x_data.append(search[i]['votes'])

        plot_div = plot([Scatter(x=x_data, y=y_data,text=z_data,
                            mode='markers', name='test',
                            opacity=0.8, marker_color='green')],
                   output_type='div')
        return render(request, "index.html", context={'plot_div': plot_div})
