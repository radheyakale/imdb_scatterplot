This project extracts data from IMDB and Plots in on a chart.

It consists of IMDbPY (opensource API for IMDB).
The graph is plotted on plotly.
Movie name gets highlighted on mouse hover.
